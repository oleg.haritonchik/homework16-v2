
#include <iostream>
#include <time.h>

using namespace std;

int main()
{
    //user defined set of array size
    cout << "Enter N: ";
    int N;
    cin >> N;
    const int SIZE = 500;   //max size of string/column

    //initialize array N * N
    int array[SIZE][SIZE];
    for (int i = 0; i < N; i++)         //fill only N string elements/
    {
        for (int j = 0; j < N; j++)     //fill only N column elements/
        {
            array[i][j] = i + j;
        }
    }

    //print array elements
    cout << "Array:\n\n";
    for (int i = 0; i < N; i++)         //print only N string elements/
    {
        for (int j = 0; j < N; j++)     //print only N column elements/
        {
            {
                if (i + j < 10)                         //facilitate reading in console, works fine for 3 digits (n=59 max for fullscreen)
                {
                    cout << "  " << array[i][j] << " ";
               
                }
                else if (i + j >= 10 && i + j < 100)
                {
                    cout << " " << array[i][j] << " ";
                                    }
                else { cout << array[i][j] << " "; }
            }
        }
        cout << "\n";
    }

    cout << "\n";


    //get day V1
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
    int day = buf.tm_mday;
    cout << "Current day is : " << day << "\n";


    //calculate array string
    int Modulus = day % N;
    cout << "Modulus of Day/N : " << day << "/" << N << " = " << Modulus << "\n";


    //calculate sum
    int sum = 0;
    for (int j = 0; j < N; j++)
    {
        sum += array[Modulus][j];
    }

    //print sum
    cout << "Sum of Elements: ";
    cout << sum << "\n";



}






